#undef __Vendor_SysTickConfig

#include <string.h>
#include <stm32f10x.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include <stm32f10x_sdio.h>
#include <stm32f10x_usart.h>
#include <ff.h>
#include "sdcard.h"

struct
{
    uint32_t rcc;
    void (*rcc_periphclockcmd)(uint32_t RCC_APB2Periph, FunctionalState NewState);
    GPIO_TypeDef *gpio;
    uint16_t pin;
} led = { RCC_APB2Periph_GPIOB, RCC_APB2PeriphClockCmd, GPIOB, GPIO_Pin_0 };

void hang(void)
{
    do {} while (1);
}

SD_Error sdcard_init(void)
{
    SD_Error status = SD_Init();
    SD_CardInfo SDCardInfo;

    SD_SetDeviceMode(SD_DMA_MODE);

    if (status == SD_OK)
        status = SD_GetCardInfo(&SDCardInfo);

    if (status == SD_OK)
        status = SD_SelectDeselect((u32) (SDCardInfo.RCA << 16));

    if (status == SD_OK)
        status = SD_EnableWideBusOperation(SDIO_BusWide_4b);

    return status;
}

void usart_init(void)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

    GPIO_InitTypeDef gpio_cfg;

    gpio_cfg.GPIO_Pin = GPIO_Pin_9;
    gpio_cfg.GPIO_Speed = GPIO_Speed_2MHz;
    gpio_cfg.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &gpio_cfg);

    gpio_cfg.GPIO_Pin = GPIO_Pin_10;
    gpio_cfg.GPIO_Speed = GPIO_Speed_2MHz;
    gpio_cfg.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    GPIO_Init(GPIOA, &gpio_cfg);

    USART_InitTypeDef usart_cfg;
    USART_StructInit(&usart_cfg);
    usart_cfg.USART_BaudRate = 115200;
    USART_Init(USART1, &usart_cfg);
    USART_Cmd(USART1, ENABLE);
}

void usart_send_text(const char *text)
{
    while (*text)
    {
        USART_SendData(USART1, *text++);
        do {} while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET);
    }
}

int main(void)
{
    usart_init();
    usart_send_text("--- STM32 FATFS SDIO ---\n");

    FATFS fs;

    if (f_mount(0, &fs) != FR_OK)
        hang();

    FIL file;
    FILINFO info;

    usart_send_text("Reading x.txt... ");

    if (f_stat("x.txt", &info) != FR_OK || f_open(&file, "x.txt", FA_READ) == FR_OK)
    {
        char buf[info.fsize + 1];
        UINT bytes_read;
        FRESULT result = f_read(&file, buf, info.fsize, &bytes_read);
        f_close(&file);

        if (result != FR_OK || bytes_read != info.fsize)
        {
            usart_send_text("READ ERROR\n");
            hang();
        }

        buf[info.fsize] = 0;
        usart_send_text("OK\n");
        usart_send_text("---- CONTENT ----\n");
        usart_send_text(buf);
        usart_send_text("\n----------------\n");
    }
    else
        hang();

    usart_send_text("Writing z.txt... \n");

    if (f_open(&file, "z.txt", FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)
    {
        const char *text = "... But I, being poor, have only my dreams.\n"
                           "I have spread my dreams under your feet.\n"
                           "Tread softly because you tread on my dreams.\n"
                           "                        William Butler Yeats\n";
        const size_t text_size = strlen(text);

        UINT bytes_written;
        FRESULT result = f_write(&file, text, text_size, &bytes_written);
        f_close(&file);

        if (result != FR_OK || bytes_written != text_size)
        {
            usart_send_text("WRITE ERROR\n");
            hang();
        }
        else
            usart_send_text("OK\n");
    }
    else
        hang();

    hang();
}
